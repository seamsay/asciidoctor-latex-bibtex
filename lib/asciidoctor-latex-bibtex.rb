require 'asciidoctor'
require 'asciidoctor/extensions'

module AsciidoctorBibtex
    def self.unnamed_to_list(attrs)
        # NOTE: `attrs` are keyed by integers when there are no named parameters.
        attrs.sort().select { |item| item[0].is_a? Integer }.map { |item| item[1] }
    end

    class CitationPackagesDocinfoProcessor < Asciidoctor::Extensions::DocinfoProcessor
        def process(document)
            options = []

            if document.attr? 'latex-bibtex-backend'
                options << "backend=#{document.attr 'latex-bibtex-backend'}"
            end

            if document.attr? 'latex-bibtex-style'
                options << "style=#{document.attr 'latex-bibtex-style'}"
            elsif document.attr? 'bibtex-style'
                options << "style=#{document.attr 'bibtex-style'}"
            end

            if document.attr? 'latex-bibtex-order'
                options << "sorting=#{document.attr 'latex-bibtex-order'}"
            elsif document.attr? 'bibtex-order'
                original = document.attr 'bibtex-order'

                converted = if original == 'appearance'
                    'none'
                elsif original == 'alphabetical'
                    'nty'
                else
                    raise 'Unsupported value for `bibtex-order`.'
                end

                options << "sorting=#{converted}"
            end

            if document.attr? 'latex-bibtex-sort-in-text'
                sort = document.attr('latex-bibtex-sort-in-text')
                raise "`latex-bibtex-sort-in-text` can only be `true` or `false`!" unless ['true', 'false'].include? sort
                options << "sortcites=#{sort}"
            end

            usepackage = ['\usepackage']
            if options.length > 0
                usepackage << "[#{options.join ','}]"
            end
            usepackage << '{biblatex}'

            result = [usepackage.join('')]

            if document.attr? 'bibtex-file'
                result << "\\addbibresource{#{document.attr("bibtex-file")}}"
            else
                # Auto-detect any `.bib` files in the document's directory.
                result.concat(Dir.glob("#{document.base_dir}/*.bib").map do |file|
                    "\\addbibresource{#{file}}"
                end)
            end

            result.join Asciidoctor::LF
        end
    end

    class BibliographyBlockMacro < Asciidoctor::Extensions::BlockMacroProcessor
        use_dsl
        named :bibliography

        def process(parent, target, attrs)
            create_pass_block(parent, '\printbibliography', attrs)
        end
    end

    class BibitemInlineMacro < Asciidoctor::Extensions::InlineMacroProcessor
        use_dsl
        named :bibitem
        format :short

        def process(parent, _target, attrs)
            citations = AsciidoctorBibtex::unnamed_to_list attrs

            if citations.length != 1
                raise '`bibitem` can only process one citation per command'
            end

            "\\nocite{#{citations[0]}}"
        end
    end

    class CiteInlineMacro < Asciidoctor::Extensions::InlineMacroProcessor
        use_dsl
        named :cite
        format :short

        def process(parent, target, attrs)
            citations = AsciidoctorBibtex::unnamed_to_list attrs

            '\parencite{' + citations.join(',') + '}'
        end
    end

    class CiteNPInlineMacro < Asciidoctor::Extensions::InlineMacroProcessor
        use_dsl
        named :citenp
        format :short

        def process(parent, target, attrs)
            citations = AsciidoctorBibtex::unnamed_to_list attrs

            '\textcite{' + citations.join(',') + '}'
        end
    end
end

Asciidoctor::Extensions.register do |registry|
    if registry.document.basebackend? 'latex' or registry.document.basebackend? 'tex'
        registry.docinfo_processor AsciidoctorBibtex::CitationPackagesDocinfoProcessor
        registry.block_macro AsciidoctorBibtex::BibliographyBlockMacro
        registry.inline_macro AsciidoctorBibtex::BibitemInlineMacro
        registry.inline_macro AsciidoctorBibtex::CiteInlineMacro
        registry.inline_macro AsciidoctorBibtex::CiteNPInlineMacro
    end
end