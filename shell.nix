let
  pkgs = import <nixpkgs> { };
  environment = pkgs.bundlerEnv {
    name = "asciidoctor-latex-bibtex";

    inherit (pkgs) ruby;
    gemdir = ./.;
  };
in environment.env
