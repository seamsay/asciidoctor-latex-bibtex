##
# This is a very basic LaTeX parser which should work well enough for this project's tests.
#
# The entry-point for this module is +LaTeXParser.parse+, which produces an array of text and +LaTeXParser::Node+.
#
#     > LaTeXParser.parse('\begin{env}[two][options]{and}{arguments} some \LaTeX text {} with some \comm[a][n]{d}{s \and stuff} and \begin{nested}environments\end{nested}\end{env}')[0].pretty
#     => LaTeXParser::Environment{env}[two][options]{and}{arguments}
#         some
#         LaTeXParser::Command{LaTeX}
#         text
#         {}
#         with
#         some
#         LaTeXParser::Command{comm}[a][n]{d}{s}
#         and
#         LaTeXParser::Environment{nested}
#          environments
module LaTeXParser
    class Node
        def ==(other)
            result = other.instance_of? self.class

            if self.methods.include? :name and other.methods.include? :name
                result &= self.name == other.name
            end

            if self.methods.include? :options and other.methods.include? :options
                result &= self.options == other.options
            end

            if self.methods.include? :arguments and other.methods.include? :arguments
                result &= self.arguments == other.arguments
            end

            if self.methods.include? :children and other.methods.include? :children
                result &= self.children == other.children
            end

            result
        end

        def pretty(port = $>, indent = 0, prefix = '')
            port.print " " * indent

            port.print self.class
            self.methods.include? :name and port.puts "{#{self.name}}"

            self.methods.include? :options and self.options.each do |option|
                if option.is_a? Node
                    option.pretty(port, indent + 1, prefix + '[]')
                else
                    port.print " " * (indent + 1)
                    port.print prefix
                    port.puts "[] #{option}"
                end
            end

            self.methods.include? :arguments and self.arguments.each do |argument|
                if argument.is_a? Node
                    argument.pretty(port, indent + 1, prefix + '{}')
                else
                    port.print " " * (indent + 1)
                    port.print prefix
                    port.puts "{} #{argument}"
                end
            end

            self.methods.include? :children and self.children.each do |child|
                if child.is_a? Node
                    child.pretty(port, indent + 1)
                else
                    port.print " " * (indent + 1)
                    port.puts child
                end
            end

            nil
        end
    end

    class Command < Node
        attr_reader :name
        attr_reader :options
        attr_reader :arguments

        def initialize(name, arguments = [], options = [])
            @name = name.to_sym
            @options = options
            @arguments = arguments
        end
    end

    class Environment < Node
        attr_reader :name
        attr_reader :children
        attr_reader :options
        attr_reader :arguments

        def initialize(name, children = [], arguments = [], options = [])
            @name = name.to_sym
            @children = children
            @options = options
            @arguments = arguments
        end
    end

    def self.parse(string, position = 0)
        nodes = []

        while position < string.length
            if string[position..].start_with?('\begin')
                position, environment = parse_environment(string, position)
                nodes.concat(environment)
            elsif string[position..].start_with?('\end')
                return [position, nodes]
            elsif string[position] == '{'
                position += 1
                position, children = parse(string, position)
                position += 1
                nodes << children
            elsif string[position] == '}'
                return [position, nodes]
            elsif string[position..].start_with?('\\\\')
                nodes << '\\\\'
                position +=2
            elsif string[position] == '\\'
                position, command = parse_command(string, position)
                nodes.concat(command)
            elsif string[position..].start_with?("\n\n")
                nodes << "\n\n"
                position += 2
            elsif string[position] == "\n"
                position += 1
            elsif /\A[ \t\f\v]/ =~ string[position]
                whitespace = /\A[ \t\f\v]+/.match(string[position..])[0]
                position += whitespace.length
            elsif string[position] == '~'
                nodes << '~'
                position += 1
            else
                text = /\A[^\s\\{}]+/.match(string[position..])[0]
                nodes << text
                position += text.length
            end
        end

        nodes
    end

    def self.parse_environment(string, position = 0)
        raise "No environment at position #{position}!" unless string[position..].start_with?('\begin')
        position += '\begin'.length

        raise "Expected '{' at position #{position}!" unless string[position] == '{'
        position += 1

        name = /\A[^}]+/.match(string[position..])[0]
        position += name.length

        raise "Expected '}' at position #{position}!" unless string[position] == '}'
        position += 1

        options = []
        while string[position] == '['
            position += 1

            option = /\A[^\]]+/.match(string[position..])[0]
            options << option
            position += option.length

            raise "Expected ']' at position #{position}!" unless string[position] == ']'
            position += 1
        end

        arguments = []
        while string[position] == '{'
            position += 1

            position, children = parse(string, position)
            arguments << children

            raise "Expected '}' at position #{position}!" unless string[position] == '}'
            position += 1
        end

        position, children = parse(string, position)

        raise "Expected environment end at position #{position}!" unless string[position..].start_with?('\end')
        position += '\end'.length

        raise "Expected '{' at position #{position}!" unless string[position] == '{'
        position += 1

        end_name = /\A[^}]+/.match(string[position..])[0]
        position += end_name.length

        raise "Environment end name `#{end_name}` does not match environment name `#{name}`!" unless name == end_name

        raise "Expected '}' at position #{position}!" unless string[position] == '}'
        position += 1

        [position, [Environment.new(name, children, arguments, options)]]
    end

    def self.parse_command(string, position)
        raise "No command at position #{position}!" unless string[position] == '\\'
        position += 1

        name = /\A(\w+|[^\s])/.match(string[position..])[0]
        position += name.length

        options = []
        while string[position] == '['
            position += 1

            option = /\A[^\]]+/.match(string[position..])[0]
            options << option
            position += option.length

            raise "Expected ']' at position #{position}!" unless string[position] == ']'
            position += 1
        end

        arguments = []
        while string[position] == '{'
            position += 1

            position, children = parse(string, position)
            arguments << children

            raise "Expected '}' at position #{position}!" unless string[position] == '}'
            position += 1
        end

        [position, [Command.new(name, arguments, options)]]
    end
end