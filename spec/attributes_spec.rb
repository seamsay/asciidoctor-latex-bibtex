RSpec.describe AsciidoctorBibtex::CitationPackagesDocinfoProcessor do
    context "with no options" do
        it "generates the usepackage statement" do
            document = <<~END
                = Some Title
                :reproducible:
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:usepackage, [["biblatex"]]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with a specified backend" do
        it "gives the backend option to biblatex" do
            document = <<~END
                = Some Title
                :reproducible:
                :latex-bibtex-backend: bibtex
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:usepackage, [["biblatex"]], ["backend=bibtex"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with the latex-bibtex-style and bibtex-style option" do
        it "prioritises the latex-bibtex-style option" do
            document = <<~END
                = Some Title
                :reproducible:
                :latex-bibtex-style: numeric
                :bibtex-style: authoryear
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:usepackage, [["biblatex"]], ["style=numeric"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with the bibtex-style option" do
        it "gives the style option to biblatex" do
            document = <<~END
                = Some Title
                :reproducible:
                :bibtex-style: authoryear
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:usepackage, [["biblatex"]], ["style=authoryear"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with the latex-bibtex-order option and the bibtex-order option" do
        it "prioritises the latex-bibtex-order option" do
            document = <<~END
                = Some Title
                :reproducible:
                :latex-bibtex-order: anyvt
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:usepackage, [["biblatex"]], ["sorting=anyvt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with the bibtex-order option set to appearance" do
        it "converts the value and gives the sorting option to biblatex" do
            document = <<~END
                = Some Title
                :reproducible:
                :bibtex-order: appearance
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:usepackage, [["biblatex"]], ["sorting=none"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with the bibtex-order option set to alphabetical" do
        it "converts the value and gives the sorting option to biblatex" do
            document = <<~END
                = Some Title
                :reproducible:
                :bibtex-order: alphabetical
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:usepackage, [["biblatex"]], ["sorting=nty"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with the bibtex-file option" do
        it "uses \\addbibresource on the file" do
            document = <<~END
                = Some Title
                :reproducible:
                :bibtex-file: foo.bib
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:usepackage, [["biblatex"]]),
                LaTeXParser::Command.new(:addbibresource, [["foo.bib"]]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with the latex-bibtex-sort-in-text option" do
        it "sets sortcites to true" do
            document = <<~END
                = Some Title
                :reproducible:
                :latex-bibtex-sort-in-text: true
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:usepackage, [["biblatex"]], ["sortcites=true"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end
end