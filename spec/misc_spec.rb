RSpec.describe AsciidoctorBibtex::CitationPackagesDocinfoProcessor do
    context "the bibliography block macro" do
        it "generates a \\printbibliography command" do
            document = <<~END
                bibliography::[]
            END

            ast = [
                LaTeXParser::Command.new(:printbibliography),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, backend: 'latex'))).to eq(ast)
        end
    end

    context "the bibitem inline macro" do
        it "generates a \\nocite command" do
            document = <<~END
                bibitem:[Lane12]
            END

            ast = [
                LaTeXParser::Command.new(:nocite, [["Lane12"]])
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, backend: 'latex'))).to eq(ast)
        end
    end

    context "the bibitem inline macro" do
        it "generates a \\nocite command" do
            document = <<~END
                bibitem:[Lane12]
            END

            ast = [
                LaTeXParser::Command.new(:nocite, [["Lane12"]])
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, backend: 'latex'))).to eq(ast)
        end
    end

    context "the cite inline macro" do
        it "generates a \\parencite command" do
            document = <<~END
                cite:[Lane12]
            END

            ast = [
                LaTeXParser::Command.new(:parencite, [["Lane12"]])
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, backend: 'latex'))).to eq(ast)
        end

        xit "generates a \\parencite command with page numbers" do
            document = <<~END
                cite:[Lane12(59)]
            END

            ast = [
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, backend: 'latex'))).to eq(ast)
        end

        it "generates a \\parencite command with multiple references" do
            document = <<~END
                cite:[Lane12, Lane11]
            END

            ast = [
                LaTeXParser::Command.new(:parencite, [["Lane12,Lane11"]])
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, backend: 'latex'))).to eq(ast)
        end
    end

    context "the citenp inline macro" do
        it "generates a \\textcite command" do
            document = <<~END
                citenp:[Lane12]
            END

            ast = [
                LaTeXParser::Command.new(:textcite, [["Lane12"]])
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, backend: 'latex'))).to eq(ast)
        end

        xit "generates a \\textcite command with page numbers" do
            document = <<~END
                citenp:[Lane12(59)]
            END

            ast = [
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, backend: 'latex'))).to eq(ast)
        end

        it "generates a \\textcite command with multiple references" do
            document = <<~END
                citenp:[Lane12, Lane11]
            END

            ast = [
                LaTeXParser::Command.new(:textcite, [["Lane12,Lane11"]])
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, backend: 'latex'))).to eq(ast)
        end
    end
end