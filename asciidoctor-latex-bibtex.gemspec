begin
    require_relative 'lib/asciidoctor-latex-bibtex/version.rb'
rescue LoadError
    require 'asciidoctor-latex-bibtex/version.rb'
end

Gem::Specification.new do |spec|
    spec.name = 'asciidoctor-latex-bibtex'
    spec.version = AsciidoctorBibtex::VERSION
    spec.authors = ['Sean Marshallsay']
    spec.summary = 'The asciidoctor-bibtex macros implemented for LaTeX backend.'

    spec.files = Dir['**/**'].grep_v(/.gem$/)

    spec.require_paths = ['lib']

    spec.add_runtime_dependency('asciidoctor', '~> 2.0')

    spec.add_development_dependency('rspec', '~> 3.0')
end
